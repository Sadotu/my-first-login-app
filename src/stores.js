import { writable } from 'svelte/store';

const usersFromStorage = JSON.parse(localStorage.getItem("Users")) || [];

export const Users = writable(usersFromStorage);

Users.subscribe(value => {
    localStorage.setItem("Users", JSON.stringify(value));
});